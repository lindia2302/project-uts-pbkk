<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
</head>
<body>

    <center>
    <font color="cadetblue"> <h1>Form Data Profile</h1></font>

    </center>
    <hr>
<div class="alert alert-warning"> NPM : {{ $npm}} </div>
<div class="alert alert-success"> Nama : {{ $nama }} </div>
<div class="alert alert-info"> Prodi: {{ $prodi }} </div>
<div class="alert alert-info"> No Hp :{{ $no_hp }} </div>
<div class="alert alert-info"> Tempat dan Tanggal Lahir : {{ $tgllahir}} </div>
<div class="alert alert-info"> Jenis Kelamin: {{ $jk}} </div>
<div class="alert alert-danger"> Agama: {{$agama }}</div>
</html>
