<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <title>Form</title>
    <link rel="stylesheet" href="css/style.css">
  <style>
      body {  background-image: url('http://wallup.net/wp-content/uploads/2016/07/19/21423-simple_background.jpg');
          background-size: cover;
              background-attachment: fixed;
              background-repeat: no-repeat;}
    </style>
</head>

<body>
    <br>
    <center>
    <font color="cadetblue"> <h1>Form Data Profile</h1></font>

    </center>
    <hr>
    </div>
    <div class ="container">
		<div class="row" style="margin-top: 20px;">
	  <div class="col-6 offset-3">
	  	<div class ="card">
	  		<div class="card-header">
        <form action="/view021180077" method="post">
            @csrf

            <div class="col-auto">
                <label>NPM</label>
                <input type="text" class="form-control" name="npm">
            </div>
            <div class="col-auto">
                <label>Nama</label>
                <input type="text" class="form-control" name="nama">
            </div>
            <div class="col-auto">
                <label>Program Studi</label>
                <input type="text" class="form-control" name="prodi">
            </div>

            <div class="col-auto">
                <label>No.HP</label>
                <input type="text" class="form-control" name="no_hp">
            </div>
            <div class="col-auto">
                <label>Tempat, Tanggal Lahir</label>
                <input type="text" class="form-control" name="tgllahir">
            </div>
            <div class="col-auto">
                <label>Jenis Kelamin</label>
                <input type="text" class="form-control" name="jk">
            </div>
            <div class="col-auto">
                <label>agama</label>
                <input type="text" class="form-control" name="agama">
            </div>
            <br>
            &nbsp;&nbsp; <button class="btn btn-info" type="submit">Kirim</button>
        </form>
    </div>
    </div>
    </div>
    </div>
    </div>

</body>

</html>
